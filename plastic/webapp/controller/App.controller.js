sap.ui.define(
    [
        "sap/ui/core/mvc/Controller"
    ],
    function(BaseController) {
      "use strict";
  
      return BaseController.extend("plastic.controller.App", {
        onInit() {
        }
      });
    }
  );
  
  


//   sap.ui.define([
//     "sap/ui/core/mvc/Controller"
// ],
//     /**
//      * @param {typeof sap.ui.core.mvc.Controller} Controller
//      */
//     function (Controller) {
//         "use strict";

//         return Controller.extend("plastic.controller.Main", {
//             onInit: function () {

//             },
//             onOpenMaterialInsert: function () {
//                 var oView = this.getView();
//                 if (!this.oAddMaterialDialog) {
//                     this.oAddMaterialDialog = sap.ui.xmlfragment(oView.getId(), "plastic.view.MaterialInsert", this);
//                     oView.addDependent(this.oAddMaterialDialog);
//                 }
//                 this.oAddMaterialDialog.open();
//             },
//             closeDialog: function (oEvent) {
//                 this.oAddMaterialDialog.close();
//             },
//             onLiveChangeMaterial: function () {
//                 var abcRegex = /^[a-zA-Z]+$/;
//                 var numberRegex = /^\d+([.,]\d+)?$/;
//                 //QuantityCheck//
//                 var aPriceVal = this.getView().byId("insertQuantityId")
//                 var aPrice = aPriceVal.getValue();
//                 var isValidPrice = numberRegex.test(aPrice);
//                 aPriceVal.setValueState(isValidPrice || !aPrice ? "None" : "Error");
//                 aPriceVal.setValueStateText(isValidPrice || !aPrice ? "" : "Invalid Quantity format");
//                 // //NR Check//
//                 // var aNameVal = this.getView().byId("insertNRId")
//                 // var aName = aNameVal.getValue();
//                 // var isValidName = abcRegex.test(aName);
//                 // if (isValidName || !aName) {
//                 //     aNameVal.setValueState("None");
//                 // } else {
//                 //     aNameVal.setValueState("Error");
//                 // }
//                 // aNameVal.setValueStateText(isValidName || !aName ? "" : "Invalid NR format");
//             },
//             onInsertMaterial: function (oEvent) {
//                 var oModel = this.getView().getModel()
//                 var that = this;

//                 var aMaterialVal = this.getView().byId("insertMaterialId");
//                 var aMaterial = aMaterialVal.getValue();
//                 var aPacktypeVal = this.getView().byId("insertPacktypeId");
//                 var aPacktype = aPacktypeVal.getSelectedKey();
//                 var aQuantityVal = this.getView().byId("insertQuantityId");
//                 var aQuantity = aQuantityVal.getValue();
//                 var aNRVal = this.getView().byId("insertNRId");
//                 var aNR = aNRVal ? aNRVal.getValue() : "";

//                 var aPlantVal = this.getView().byId("insertPlantId")
//                 var aPlant = aPlantVal ? aPlantVal.getValue() : "non presente";

//                 //Error fields check
//                 if (aMaterialVal.getValueState() === "Error"
//                     || aPacktypeVal.getValueState() === "Error"
//                     || aQuantityVal.getValueState() === "Error"
//                     || aNRVal && aNRVal.getValueState() === "Error"
//                     || aPlantVal && aPlantVal.getValueState() === "Error") {
//                     sap.m.MessageToast.show("Please fill all fields correctly");
//                     return;
//                 }
//                 //empty fields check
//                 if (!aMaterial || !aPacktype || !aQuantity || !aNR || !aPlant) {
//                     sap.m.MessageToast.show("Fill Every Field");
//                     return;
//                 }

//                 var JSON = {
//                     material: aMaterial,
//                     packagingType_ID: aPacktype,
//                     packQuantity: aQuantity,
//                     nr: aNR
//                 }

//                 oModel.create('/Material', JSON, {
//                     success: function () {
//                         sap.m.MessageToast.show(" Created Successfully");
//                         that.byId("insertMaterialId").setValue("");
//                         that.byId("insertPacktypeId").setValue("");
//                         that.byId("insertQuantityId").setValue("");
//                         that.byId("insertNRId").setValue("");
//                         that.byId("insertMaterialId").close();
//                     },
//                     error: function () {
//                         sap.m.MessageToast.show(" Created Failed");
//                     }

//                 });
//             }
//         });
//     });

//     //npm add @sap/cds-odata-v2-adapter-proxy
