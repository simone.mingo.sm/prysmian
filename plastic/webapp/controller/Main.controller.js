sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller) {
        "use strict";

        return Controller.extend("plastic.controller.Main", {
            onInit: function () {

            },
            //---------------------- ADD NEW Article -----------------------//
            //-------- open fragment ---------//
            onOpenMaterialInsert: function () {
                var oView = this.getView();
                if (!this.oAddMaterialDialog) {
                    this.oAddMaterialDialog = sap.ui.xmlfragment(oView.getId(), "plastic.view.MaterialInsert", this);
                    oView.addDependent(this.oAddMaterialDialog);
                }
                this.oAddMaterialDialog.open();
            },
            closeDialog: function (oEvent) {
                this.oAddMaterialDialog.close();
            },
            //----- validate input fields -----//
            onLiveChangeMaterial: function () {
                var abcRegex = /^[a-zA-Z]+$/;
                var numberRegex = /^\d+([.,]\d+)?$/;
                //PriceCheck//
                // var aPriceVal = this.getView().byId("insertQuantityId")
                // var aPrice = aPriceVal.getValue();
                // var isValidPrice = numberRegex.test(aPrice);
                // aPriceVal.setValueState(isValidPrice || !aPrice ? "None" : "Error");
                // aPriceVal.setValueStateText(isValidPrice || !aPrice ? "" : "Invalid Quantity format");
                // //NR Check//
                // var aNameVal = this.getView().byId("insertNRId")
                // var aName = aNameVal.getValue();
                // var isValidName = abcRegex.test(aName);
                // if (isValidName || !aName) {
                //     aNameVal.setValueState("None");
                // } else {
                //     aNameVal.setValueState("Error");
                // }
                // aNameVal.setValueStateText(isValidName || !aName ? "" : "Invalid NR format");
            },
            //----- insert validated fields-----//
            onInsertMaterial: function (oEvent) {
                var oModel = this.getView().getModel()
                var that = this;

                var aMaterialVal = this.getView().byId("insertMaterialId");
                var aMaterial = aMaterialVal.getValue();
                var aPacktypeVal = this.getView().byId("insertPacktypeId");
                var aPacktype = aPacktypeVal.getSelectedKey();
                var aQuantityVal = this.getView().byId("insertQuantityId");
                var aQuantity = aQuantityVal.getValue();
                var aNRVal = this.getView().byId("insertNRId");
                var aNR = aNRVal.getValue();
                var aPeriodVal = this.getView().byId("insertPeriodId");
                var aPeriod = aPeriodVal.getDateValue().toISOString().split('T')[0];


                //Error fields check
                if (aMaterialVal.getValueState() === "Error"
                    || aPacktypeVal.getValueState() === "Error"
                    || aQuantityVal.getValueState() === "Error"
                    || aNRVal.getValueState() === "Error"
                    || aPeriodVal.getValueState() === "Error") {
                    sap.m.MessageToast.show("Please fill all fields correctly");
                    return;
                }
                //empty fields check
                if (!aMaterial || !aPacktype || !aQuantity || !aNR || !aPeriod) {
                    sap.m.MessageToast.show("Fill Every Field");
                    return;
                }

                //-----temporany model------//
                var JSON = {
                    material: aMaterial,
                    packagingType_ID: aPacktype,
                    period:aPeriod,
                    packQuantity: aQuantity,
                    nr: aNR
                }

                oModel.create('/Material', JSON, {
                    success: function () {
                        sap.m.MessageToast.show(" Created Successfully");
                        that.byId("insertMaterialId").setValue("");
                        that.byId("insertPacktypeId").setValue("");
                        that.byId("insertQuantityId").setValue("");
                        that.byId("insertPeriodId").setValue("");
                        that.byId("insertNRId").setValue("");
                        that.oAddMaterialDialog.close()
                    },
                    error: function () {
                        sap.m.MessageToast.show(" Created Failed");
                    }

                });
            },

        //---------------EDIT Material---------------//
        onOpenEditMaterial: function (oEvent) {
            var oView = this.getView();
        
            //prendiamo l' elemento della lista
            var oListItem = oEvent.getSource().getParent();
        
            // If the fragment doesn't exist, create it
            if (!this.oDialog) {
                this.oDialog = sap.ui.xmlfragment(oView.getId(), "plastic.view.MaterialEdit", this);
                oView.addDependent(this.oDialog);
            }
        
            // Prendiamo l'ID dell' utente dalla lista nel contesto di binding associato all' elemento
            var aMaterialId = oListItem.getBindingContext().getProperty("ID");
        
            //prendo i valori pre-esistenti dell'oEvent cliccato
            var aMaterial;
            var aMaterial = oListItem.getBindingContext().getProperty("material");
            var aPeriod = oListItem.getBindingContext().getProperty("period");
            var aQuantity = oListItem.getBindingContext().getProperty("packQuantity");
            var aNR = oListItem.getBindingContext().getProperty("nr");
        
            
        
            //li metto in un modello di appoggio
            var oModel = new sap.ui.model.json.JSONModel({
                materialId: aMaterialId,
                material: aMaterial,
                period: aPeriod,
                packQuantity: aQuantity,
                nr: aNR,
            });
        
            //associamo ad oModel l' oggetto user per poterne utilizzare i dati e popolare l' interfaccia utente
            this.oDialog.setModel(oModel, "material");
        
            // Open the fragment
            this.oDialog.open();
        },
        
        closeEdit: function (oEvent) {
            this.oDialog.close();
        },
        updateMaterial: function (oEvent) {
            var that = this;
            var oModel = this.getView().getModel();
            // Get the dialog from the event source (the element that triggered the event)
            var oDialog = oEvent.getSource().getParent();
        
            // Get the material ID from the dialog's model data
            var edMaterialId = oDialog.getModel("material").getProperty("/materialId");
            
            // Get the updated material properties
            var edMaterialVal = this.getView().byId("editMaterialId");
            var edMaterial = edMaterialVal.getValue();
        
            var edPeriodVal = this.getView().byId("editPeriodId");
            var edPeriod = edPeriodVal.getValue();
        
            var edQuantityVal = this.getView().byId("editQuantityId");
            var edQuantity = edQuantityVal.getValue();
        
            var edNRVal = this.getView().byId("editNRId");
            var edNR = edNRVal.getValue();
        
            // Check for errors in the input fields
            if(edMaterialVal.getValueState() === "Error" || 
               edPeriodVal.getValueState() === "Error" || 
               edQuantityVal.getValueState() === "Error" || 
               edNRVal.getValueState() === "Error") {
                sap.m.MessageToast.show("Please fill all fields correctly");
                return;
            }
        
            // Check for empty fields
            if (!edMaterial || !edPeriod || !edQuantity || !edNR) {
                sap.m.MessageToast.show("Fill Every Field");
                return;
            }
        
            // Create the updated material object
            var JSON = {
                material: edMaterial,
                period: edPeriod,
                packQuantity: edQuantity,
                nr: edNR
            }
        
            // Update the material in the model
            oModel.update("/Material(guid'" + edMaterialId + "')", JSON, {
                success: function () {
                    sap.m.MessageToast.show(" Updated Successfully");
                    that.byId("editMaterialId").setValue("");
                    that.byId("editPeriodId").setValue("");
                    that.byId("editQuantityId").setValue("");
                    that.byId("editNRId").setValue("");
                    that.byId("idEdit").close();
                },
                error: function () {
                    sap.m.MessageToast.show("Update Failed");
                }
            })
        }
        
        
            
        });
    });

    //npm add @sap/cds-odata-v2-adapter-proxy
