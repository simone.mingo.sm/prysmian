/* global QUnit */

sap.ui.require(["plastic/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
