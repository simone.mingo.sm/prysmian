namespace my.company;

entity PackagingGroup
{
    key ID : UUID
        @Core.Computed;
    packGroup : String;
    description : String;
    packagingType : Association to many PackagingType on packagingType.packagingGroup = $self;
}

entity PackagingType
{
    key ID : UUID
        @Core.Computed;
    packType : String;
    plant : String;
    taxBase : Decimal;
    currency : String;
    per : Integer;
    uom : String;
    periodFrom : Date;
    periodTo : Date;
    packagingGroup : Association to one PackagingGroup;
    material : Association to many Material on material.packagingType = $self;
}

entity Material
{
    key ID : UUID
        @Core.Computed;
    material : String;
    packQuantity : Decimal;
    nr : String;
    period:DateTime;
    packagingType : Association to one PackagingType;
}
