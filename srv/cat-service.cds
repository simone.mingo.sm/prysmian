using my.company as my from '../db/data-model';

service CatalogService {
    entity PackagingGroup as projection on my.PackagingGroup;
    entity PackagingType as projection on my.PackagingType;
    entity Material as projection on my.Material;

}